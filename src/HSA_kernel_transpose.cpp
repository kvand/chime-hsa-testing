#include "HSA_kernel_transpose.hpp"
#include <unistd.h>
#include <string.h>

HSA_kernel_transpose::HSA_kernel_transpose(HSA_device *device)
    : HSA_kernel(device)
{
	    /* Load the kernel object. */
    this->kernel_object = this->load_hsaco_file("kernels/transpose.hsaco", "transpose");
}

void HSA_kernel_transpose::initialize_memory(){
//    this->hsa_status=hsa_amd_memory_pool_allocate(this->device->host_region, N_ELEM*N_ITER*2*sizeof(float), 0, (void**)&this->output);
//    assert(this->hsa_status == HSA_STATUS_SUCCESS);
//    memset(this->output,0,N_ELEM*N_ITER*2*sizeof(float));

//    this->sync_copy_host_to_gpu(this->input_buffer, this->input, N_ELEM * N_ITER);
//    this->sync_copy_host_to_gpu(this->output_buffer, this->output, N_ITER * N_ELEM);
}

void HSA_kernel_transpose::allocate_hsa_memory(void **mem_pointers){
    this->gpu_allocate((void**)&this->input_buffer, N_ELEM * N_ITER * 4 * sizeof(float), mem_pointers[0]);
    this->gpu_allocate((void**)&this->output_buffer, N_ITER * N_ELEM * 4 * sizeof(float), mem_pointers[1]);

    this->hsa_status = hsa_memory_allocate(device->kernarg_region,16, &this->kernarg_address);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);
}

void **HSA_kernel_transpose::get_hsa_memory(){
/*  void *mem[5] = {
    (void*)this->blk_map,
    (void*)this->input_buffer,
    (void*)this->presum_buffer,
    (void*)this->corr_buffer,
    (void*)this->cfg_buffer
  };
  return mem;*/
  return NULL;
}

HSA_kernel_transpose::~HSA_kernel_transpose(){

    this->hsa_status = hsa_memory_free(this->kernarg_address);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);

//    this->hsa_status = hsa_amd_memory_pool_free(this->input_buffer);
//    assert(this->hsa_status == HSA_STATUS_SUCCESS);
}

void HSA_kernel_transpose::enqueue(hsa_signal_t *signal){
    struct __attribute__ ((aligned(16))) args_t {
        void* input_buffer;
        void* output_buffer;
    } args;
    memset(&args, 0, sizeof(args));
    args.input_buffer=this->input_buffer;
    args.output_buffer=this->output_buffer;

    // Allocate the kernel argument buffer from the correct region.
    memcpy(this->kernarg_address, &args, sizeof(args));

    // Obtain the current queue write index.
    uint64_t index = hsa_queue_load_write_index_acquire(this->device->queue);
    hsa_kernel_dispatch_packet_t* dispatch_packet = (hsa_kernel_dispatch_packet_t*)this->device->queue->base_address +
                                                            (index % this->device->queue->size);
    dispatch_packet->setup  |= 2 << HSA_KERNEL_DISPATCH_PACKET_SETUP_DIMENSIONS;
    dispatch_packet->workgroup_size_x = (uint16_t)32;
    dispatch_packet->workgroup_size_y = (uint16_t)8;
    dispatch_packet->grid_size_x = (uint32_t)N_ELEM;
    dispatch_packet->grid_size_y = (uint32_t)N_ITER/4;
    dispatch_packet->completion_signal = *signal;
    dispatch_packet->kernel_object = this->kernel_object;
    dispatch_packet->kernarg_address = (void*) this->kernarg_address;
    dispatch_packet->private_segment_size = 0;
    dispatch_packet->group_segment_size = 8192;
    dispatch_packet-> header =
      (HSA_PACKET_TYPE_KERNEL_DISPATCH << HSA_PACKET_HEADER_TYPE) |
      (1 << HSA_PACKET_HEADER_BARRIER) |
      (HSA_FENCE_SCOPE_SYSTEM << HSA_PACKET_HEADER_ACQUIRE_FENCE_SCOPE) |
      (HSA_FENCE_SCOPE_SYSTEM << HSA_PACKET_HEADER_RELEASE_FENCE_SCOPE);

    hsa_queue_add_write_index_acquire(this->device->queue, 1);
    hsa_signal_store_relaxed(this->device->queue->doorbell_signal, index);
}


int HSA_kernel_transpose::copy_output_data(){
/*
    hsa_signal_t sig;
    this->hsa_status = hsa_signal_create(1, 0, NULL, &sig);
        assert(this->hsa_status == HSA_STATUS_SUCCESS);
    hsa_amd_agents_allow_access(1, &this->device->agent, NULL, this->output);
    hsa_amd_agents_allow_access(1, &this->device->cpu_agent, NULL, this->output_buffer);
    this->hsa_status = hsa_amd_memory_async_copy(this->output, this->device->cpu_agent,
                                                 this->output_buffer, this->device->agent,
                                                 N_ELEM*2*sizeof(int), 0, NULL, sig);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);
        hsa_signal_wait_acquire(sig, HSA_SIGNAL_CONDITION_LT, 1, UINT64_MAX, HSA_WAIT_STATE_BLOCKED);
        this->hsa_status=hsa_signal_destroy(sig);
        assert(this->hsa_status == HSA_STATUS_SUCCESS);
*/
    return 0;
}

int HSA_kernel_transpose::verify_output(int spotcheck){
//  this->copy_output_data();
  int errct=0;
  if (spotcheck <= 0) {
    printf("Running full check, could take a while...\n");
    int err=0;
/*
    for (int i=0; i<16; i++) {
      int re_accum=0;
      int im_accum=0;
      for (int t=0; t<N_ITER; t++) {
        im_accum+=(this->input[t*N_ELEM+i] & 0x0f) <<3;//    *8; //IM
        re_accum+=(this->input[t*N_ELEM+i] & 0xf0) >>1;// >>4*8; //RE
      }
      if ((re_accum != this->presum[i*2 + 1]) ||
          (im_accum != this->presum[i*2 + 0])) {
        printf("%i %i : %i %i\n",re_accum,im_accum, this->presum[i*2 + 0], this->presum[i*2 + 1]);
        errct++;
      }
    }
    */
  } else {
    printf("Checking %i accumulations at random...\n", spotcheck);
    for (int i=0; i<spotcheck; i++){
/*
      int x=rand() * 1.0 / RAND_MAX * 32;
      int re_accum=0;
      int im_accum=0;
      for (int t=0; t<N_ITER; t++) {
        im_accum+=(this->input[t*N_ELEM+x] & 0x0f) <<3;//    *8; //IM
        re_accum+=(this->input[t*N_ELEM+x] & 0xf0) >>1;// >>4*8; //RE
      }
      if ((re_accum != this->presum[x*2 + 1]) ||
          (im_accum != this->presum[x*2 + 0])) {
        printf("%i %i : %i %i\n",re_accum,im_accum, this->presum[x*2 + 0], this->presum[x*2 + 1]);
        errct++;
      }
    */
    }
  }
  if (errct > 0) printf("   Error! CPU does not match GPU...\n");
  else printf("   Success! CPU matches GPU!\n");

  return errct;
}



