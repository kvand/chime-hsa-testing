#pragma once
#include "HSA_kernel.hpp"
#define N_ELEM 2048
#define N_ITER 32768
#define N_INTG 16384
#define N_BLK ((N_ELEM/32*(N_ELEM/32+1))/2)

struct kernel_config_t {
  uint32_t n_elem;
  uint32_t n_intg;
  uint32_t n_iter;
  uint32_t n_blk;
};

class HSA_kernel_N2 : public HSA_kernel {
	public:
		HSA_kernel_N2(HSA_device *device);
		~HSA_kernel_N2();
		void enqueue(hsa_signal_t*);

		int copy_output_data();
		int verify_output(int spotcheck = 0);
		void allocate_hsa_memory(void **);
		void initialize_memory();
		void **get_hsa_memory();

		uint32_t *presum;
		int *output;
		char *input;
	    uint32_t *host_blk_map;

	    unsigned int* blk_map;
	    unsigned int* input_buffer;
	    unsigned int* presum_buffer;
	    int* corr_buffer;
	    void *cfg_buffer;
	    void *kernarg_address;
	    kernel_config_t* kernel_config;

	    int nkern;
};
