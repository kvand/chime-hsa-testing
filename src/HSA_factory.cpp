#include "HSA_factory.hpp"

HSA_factory::HSA_factory(int gpu_idx){
    this->hsa_status = hsa_init();
    assert(this->hsa_status == HSA_STATUS_SUCCESS);

    HSA_device *next = new HSA_device(gpu_idx);
    this->devices.push_back(next);

    this->hsa_status=hsa_amd_memory_pool_allocate(next->host_region, N_ELEM * N_ITER*sizeof(char), 0, (void**)&this->input_data);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);
    for (int i=0; i<N_ELEM*N_ITER; i++) this->input_data[i]=rand();

    HSA_kernel_presum *kern_presum = new HSA_kernel_presum(next);
    HSA_kernel_N2 *kern_N2 = new HSA_kernel_N2(next);
    HSA_kernel_transpose *kern_trsps = new HSA_kernel_transpose(next);
    kern_presum->input = this->input_data;
    kern_N2->input = this->input_data;
    kern_trsps->input = this->input_data;

    void *input_buf;
    this->hsa_status=hsa_amd_memory_pool_allocate(next->global_region, N_ELEM*N_ITER, 0, (void**)&input_buf);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);

    void *presum_buf;
    this->hsa_status=hsa_amd_memory_pool_allocate(next->global_region, N_ELEM*2*sizeof(int), 0, (void**)&presum_buf);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);

    void *trsps_in_buf;
    this->hsa_status=hsa_amd_memory_pool_allocate(next->global_region, N_ITER*N_ELEM*2*sizeof(float), 0, (void**)&trsps_in_buf);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);

    void *trsps_out_buf;
    this->hsa_status=hsa_amd_memory_pool_allocate(next->global_region, N_ITER*N_ELEM*2*sizeof(float), 0, (void**)&trsps_out_buf);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);

    void *ex_ps[2] = {input_buf, presum_buf};
    void *ex_N2[5] = {NULL, input_buf, presum_buf, NULL, NULL};
    void *ex_tr[2] = {trsps_in_buf, trsps_out_buf};

    kern_presum->allocate_hsa_memory(ex_ps);
    kern_N2->allocate_hsa_memory(ex_N2);
    kern_trsps->allocate_hsa_memory(ex_tr);

    kern_presum->initialize_memory();
    kern_N2->initialize_memory();
    kern_trsps->initialize_memory();

    this->kernels.push_back(kern_presum);
    this->kernels.push_back(kern_N2);
//    this->kernels.push_back(kern_trsps);
}

HSA_factory::~HSA_factory(){
    for (HSA_kernel*kernel : this->kernels) delete kernel;
    this->kernels.clear();

    for (HSA_device*device : this->devices) delete device;
    this->devices.clear();

    hsa_amd_memory_pool_free(this->input_data);

    this->hsa_status=hsa_shut_down();
    assert(this->hsa_status == HSA_STATUS_SUCCESS);

    printf("Shutting down!\n");
}


int HSA_factory::enqueue(){
    for (HSA_kernel*kernel : this->kernels) {
        hsa_signal_t kern_signal;
        this->hsa_status=hsa_signal_create(1, 0, NULL, &kern_signal);
        assert(this->hsa_status == HSA_STATUS_SUCCESS);

        kernel->enqueue(&kern_signal);
        hsa_signal_value_t value = hsa_signal_wait_acquire(kern_signal, HSA_SIGNAL_CONDITION_LT, 1, UINT64_MAX, HSA_WAIT_STATE_BLOCKED);

        this->hsa_status=hsa_signal_destroy(kern_signal);
        assert(this->hsa_status == HSA_STATUS_SUCCESS);
        //kernel->verify_output(1000);
    }

    return 0;
}
