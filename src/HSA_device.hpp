#pragma once
#include <list>
#include <cassert>
#include <stdio.h>
#include "hsa/hsa.h"
#include "hsa/hsa_ext_finalize.h"
#include "hsa/hsa_ext_amd.h"

struct gpu_config_t {
  int gpu_idx;
  hsa_agent_t *agent;
};

struct gpu_mem_config_t {
  int gpu_idx;
  hsa_amd_memory_pool_t *region;
};

class HSA_device{
    public:
        HSA_device(int num_gpu);
        ~HSA_device();
        float get_card_tflops();

        hsa_agent_t agent;
        hsa_amd_memory_pool_t global_region;
        hsa_region_t kernarg_region;
        hsa_queue_t* queue; 

        hsa_status_t hsa_status;

        hsa_agent_t cpu_agent;
        hsa_amd_memory_pool_t host_region;


    private:
        hsa_amd_agent_info_t compute_count;
        hsa_amd_agent_info_t clock_freq_mhz;

        int gpu_idx;
        float card_tflops;
        char agent_name[64];

        static hsa_status_t get_gpu_agent(hsa_agent_t agent, void *data);
        static hsa_status_t get_kernarg_memory_region(hsa_region_t region, void* data);
        static hsa_status_t get_device_memory_region(hsa_amd_memory_pool_t region, void* data);

};
