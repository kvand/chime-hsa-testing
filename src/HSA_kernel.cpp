#include "HSA_kernel.hpp"

HSA_kernel::HSA_kernel(HSA_device *device){
  this->device = device;
}

HSA_kernel::~HSA_kernel(){
}

void HSA_kernel::gpu_allocate(void **ptr, int length, void *existing){
  if (existing) *ptr = existing;
  else {
    this->hsa_status=hsa_amd_memory_pool_allocate(this->device->global_region, length, 0, ptr);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);
    existing = ptr;
  }
}

void HSA_kernel::host_allocate(void **ptr, int length, void *existing){
  if (existing) *ptr = existing;
  else {
    this->hsa_status=hsa_amd_memory_pool_allocate(this->device->global_region, length, 0, ptr);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);
    existing = ptr;
  }
}

void HSA_kernel::sync_copy_host_to_gpu(void *dst, void *src, int length){
    hsa_signal_t sig;
    this->hsa_status = hsa_signal_create(1, 0, NULL, &sig);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);
    hsa_amd_agents_allow_access(1, &this->device->agent, NULL, src);
    hsa_amd_agents_allow_access(1, &this->device->cpu_agent, NULL, dst);
    this->hsa_status = hsa_amd_memory_async_copy(dst, this->device->agent,
                                                 src, this->device->cpu_agent, 
                                                 length, 0, NULL, sig);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);
    while(hsa_signal_wait_acquire(sig, HSA_SIGNAL_CONDITION_LT, 1, UINT64_MAX, HSA_WAIT_STATE_ACTIVE));
    this->hsa_status=hsa_signal_destroy(sig);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);
}


void HSA_kernel::sync_copy_gpu_to_host(void *dst, void *src, int length){
    hsa_signal_t sig;
    this->hsa_status = hsa_signal_create(1, 0, NULL, &sig);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);
    hsa_amd_agents_allow_access(1, &this->device->cpu_agent, NULL, src);
    hsa_amd_agents_allow_access(1, &this->device->agent, NULL, dst);
    this->hsa_status = hsa_amd_memory_async_copy(dst, this->device->cpu_agent,
                                                 src, this->device->agent, 
                                                 length, 0, NULL, sig);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);
    while(hsa_signal_wait_acquire(sig, HSA_SIGNAL_CONDITION_LT, 1, UINT64_MAX, HSA_WAIT_STATE_ACTIVE));
    this->hsa_status=hsa_signal_destroy(sig);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);
}

uint64_t HSA_kernel::load_hsaco_file(const char *fileName, const char *kernelName)
{
/* Extract the code handle for the specified kernelName from the specified fileName
   Returns a 64-bit code object which can be used with an AQL packet */

  // Open file.
  std::ifstream file(fileName, std::ios::in | std::ios::binary);
  assert(file.is_open() && file.good());

  // Find out file size.
  file.seekg(0, file.end);
  size_t code_object_size = file.tellg();
  file.seekg(0, file.beg);

  // Allocate memory for raw code object.
  char *raw_code_object = (char*)malloc(code_object_size);
  assert(raw_code_object);

  // Read file contents.
  file.read(raw_code_object, code_object_size);

  // Close file.
  file.close();

  // Deserialize code object.
  hsa_code_object_t code_object = {0};
  this->hsa_status = hsa_code_object_deserialize((void*)raw_code_object, code_object_size, NULL, &code_object);
  assert(HSA_STATUS_SUCCESS == this->hsa_status);
  assert(0 != code_object.handle);

  // Create executable.
  hsa_executable_t hsaExecutable;
  this->hsa_status = hsa_executable_create(HSA_PROFILE_FULL, HSA_EXECUTABLE_STATE_UNFROZEN, NULL, &hsaExecutable);
  assert(HSA_STATUS_SUCCESS == this->hsa_status);

  // Load code object.
  this->hsa_status = hsa_executable_load_code_object(hsaExecutable, this->device->agent, code_object, NULL);
  assert(HSA_STATUS_SUCCESS == this->hsa_status);

  // Freeze executable.
  this->hsa_status = hsa_executable_freeze(hsaExecutable, NULL);
  assert(HSA_STATUS_SUCCESS == this->hsa_status);

  // Get symbol handle.
  hsa_executable_symbol_t kernelSymbol;
  this->hsa_status = hsa_executable_get_symbol(hsaExecutable, NULL, kernelName, this->device->agent, 0, &kernelSymbol);
  assert(HSA_STATUS_SUCCESS == this->hsa_status);

/*
    uint32_t kernarg_segment_size;
    uint32_t group_segment_size;
    uint32_t private_segment_size;
    this->hsa_status = hsa_executable_symbol_get_info(kernelSymbol, HSA_EXECUTABLE_SYMBOL_INFO_KERNEL_KERNARG_SEGMENT_SIZE, &kernarg_segment_size);
  assert(HSA_STATUS_SUCCESS == this->hsa_status);
    printf("Kernarg Segment Size: %i\n",kernarg_segment_size);
    this->hsa_status = hsa_executable_symbol_get_info(kernelSymbol, HSA_EXECUTABLE_SYMBOL_INFO_KERNEL_GROUP_SEGMENT_SIZE,&group_segment_size);
  assert(HSA_STATUS_SUCCESS == this->hsa_status);
    printf("Group Segment Size: %i\n",group_segment_size);
    this->hsa_status = hsa_executable_symbol_get_info(kernelSymbol, HSA_EXECUTABLE_SYMBOL_INFO_KERNEL_PRIVATE_SEGMENT_SIZE, &private_segment_size);
  assert(HSA_STATUS_SUCCESS == this->hsa_status);
    printf("Private Segment Size: %i\n",private_segment_size);
*/

  // Get code handle.
  uint64_t codeHandle;
  this->hsa_status = hsa_executable_symbol_get_info(kernelSymbol, HSA_EXECUTABLE_SYMBOL_INFO_KERNEL_OBJECT, &codeHandle);
  assert(HSA_STATUS_SUCCESS == this->hsa_status);

  // Free raw code object memory.
  free((void*)raw_code_object);

  return codeHandle;
};
