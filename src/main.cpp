#include <stdio.h>
#include <stdlib.h>
#include "HSA_factory.hpp"

#define N_ELEM 2048
#define N_ITER 32768
#define N_INTG 16384
#define N_BLK ((N_ELEM/32*(N_ELEM/32+1))/2)

double e_time(void)
{
  static struct timeval now;
  gettimeofday(&now, NULL);
  return (double)(now.tv_sec  + now.tv_usec/1000000.0);
}

int main(int argc, char **argv) {
  extern char *optarg;
  extern int optind;

  int gpu_idx=0;
  bool do_verify=false;
  int nkern=1;
  char c;
  while ((c = getopt(argc, argv, "d:vn:")) != -1)
    switch (c) {
      case 'd':
        gpu_idx = strtol(optarg,NULL,0);
        break;
      case 'v':
        do_verify=true;
        break;
      case 'n':
        nkern = strtol(optarg,NULL,0);
        break;
    }
    srand(time(NULL));

    HSA_factory *hsa_controller = new HSA_factory(gpu_idx); //just fire up the first GPU

    double time_available = 2.56e-6 * N_ITER;

    double totaltime = e_time();

    for (int i=0; i<(uint)nkern; i++){
      double cputime = e_time();
      hsa_controller->enqueue();
      cputime = e_time()-cputime;
      printf("Time: %6.4fs",cputime);
      printf(" (i.e. %4.1f%% of %6.4fs available.)\n",cputime/time_available*100,time_available);

      double dt=time_available-cputime;
      if (dt > 0) usleep(dt * 1e6);
    }
    totaltime = e_time()-totaltime;
    printf("Total Time: %6.4fs (%6.4f/iteration)\n",totaltime,totaltime/nkern);

    delete hsa_controller;

    return 0;
}
